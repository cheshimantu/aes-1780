import allure
from pytest import mark


@mark.parametrize('test_case_number', [
    '91048',
    '91049'
])
class TestParametrize:
    @allure.id('{test_case_number}')
    #@allure.label('as_id', '{test_case_number}')
    @allure.title('{test_case_number} - test_parametrize')
    def test_parametrize(self, test_case_number):
        print(test_case_number)
        assert 1 == 1 