import allure
from pytest import mark


@mark.parametrize('test_case_number', [
    '91046',
    '91047'
])
class TestDynamic:
    @allure.title('{test_case_number} - test_dynamic')
    def test_dynamic(self, test_case_number):
        allure.dynamic.label('as_id', test_case_number)
        print(test_case_number)
        assert 1 == 1 